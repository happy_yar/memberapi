<?php

namespace App\Providers;

use Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('result', function($model)
        {
            return (!$model)?Response::make('Not Found',404):$model;
        });

        Response::macro('dummy', function()
        {
            return Response::make('Not Implemented',501);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
