<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('member',['uses'=>'MemberController@index','middleware' => 'auth']);
    Route::get('event','EventController@index');
    Route::get('event/create', 'EventController@create');
    Route::get('event/del/{id}', 'EventController@destroy');
    Route::get('member/del/{id}', 'MemberController@destroy');
    Route::post('event/store', 'EventController@store');
    Route::get('event/{id}', 'EventController@show');
    Route::get('event/edit/{id}','EventController@edit');
    Route::post('event/update/{id}', [
        'as' => 'event.update', 'uses' => 'EventController@update'
    ]);
    Route::post('member/update/{id}', [
        'as' => 'member.update', 'uses' => 'MemberController@update'
    ]);
    Route::post('member/ajax','MemberController@store');
    Route::get('member/join/{id}','MemberController@join');
    Route::get('member/edit/{id}','MemberController@edit',['middleware' => 'auth']);
    Route::get('member/{id}', 'MemberController@show',['middleware' => 'auth']);
    Route::get('/','MemberController@create');

});

Route::get('rest/events/nearest', ['uses'=>'Rest\EventsController@nearest','middleware'=>'rest.auth']);
Route::resource('rest/members', 'Rest\MembersController');
Route::resource('rest/events', 'Rest\EventsController');


