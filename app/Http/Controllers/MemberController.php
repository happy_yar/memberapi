<?php

namespace App\Http\Controllers;

use App\Event;
use App\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\URL;

class MemberController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $models = Member::all();

        return view('member.list',['models'=>$models]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'event_id' => 'required|unique:members,event_id,NULL,id,email,'.$request->email,
            'phone' => 'Regex:/^\+7[0-9]*$/',
            'birthday' => 'date',
        ]);

        $member = new Member;
        $member->name = $request->input('name');
        $member->email = $request->input('email');
        $member->phone = $request->input('phone');
        $member->birthday = $request->input('birthday');
        $member->male = $request->input('male');
        $member->event_id = $request->input('event_id');

        $member->save();

        return "OK";
    }

    public function join($id){
        $event = Event::find($id);
        return view('member.join',['event'=>$event]);
    }


    public function create(){

        return view('member.form');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Member::findOrFail($id);

        return view('member.show',['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Member::find($id);
        $events = Event::all();
        return view('member.edit',['model'=>$model,'events'=>$events]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'event_id' => 'required',
            'phone' => 'Regex:/^\+7[0-9]*$/',
            'birthday' => 'date',
        ]);

        $member = Member::find($id);
        $member->name = $request->input('name');
        $member->email = $request->input('email');
        $member->phone = $request->input('phone');
        $member->birthday = $request->input('birthday');
        $member->male = $request->input('male');
        $member->event_id = $request->input('event_id');

        $member->save();
        \Session::flash('flash-message','Участник сохранен'); //<--FLASH MESSAGE
        return redirect('/member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Member::find($id);
        $model->delete();
        \Session::flash('flash-message','Участник удален');
        return redirect('/member');
    }
}
