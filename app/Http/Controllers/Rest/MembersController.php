<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Member;
use Validator;

class MembersController extends Controller
{

    public function __construct()
    {
        $this->middleware('rest.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Member::all()->toArray();

        return response()->result($models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->dummy();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = 200;
        $validation = null;

        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'event_id' => 'required|exists:events,id|unique:members,event_id,NULL,id,email,'.$request->email,
            'phone' => 'Regex:/^\+7[0-9]*$/',
            'birthday' => 'date',
        ]);

        if ($v->fails())
        {

            $status = 422;
            $message = 'Ошибки валидации';
            $validation = $v->errors();

        }
        else{

            $validation = null;
            $message = "Участник зарегистрирован";
            $member = new Member;
            $member->name = $request->input('name');
            $member->email = $request->input('email');
            $member->phone = $request->input('phone');
            $member->birthday = $request->input('birthday');
            $member->male = $request->input('male');
            $member->event_id = $request->input('event_id');

            if( $member->save() )
                $status = 201;
            else{
                $status = 422;
                $message = 'Объект не сохранен';
            }


        }


        return response()->json(['message' => $message,'validation_errors'=>$validation],$status);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->dummy();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->dummy();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response()->dummy();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->dummy();
    }
}
