<?php

namespace App\Http\Controllers;

use App\Event;
use App\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return view('event.list',['events'=>$events]);
    }

    public function show($id){
        
        $model = Event::findOrFail($id);

        return view('event.show',['model'=>$model]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('event.form');
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Event::find($id);
        return view('event.edit',['model'=>$model]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:255',
            'place' => 'required',
            'event_date' => 'required',
            'event_time' => 'required',
            'description' => 'required'
        ]);

        $model = new Event;
        $model->title = $request->input('title');
        $model->place = $request->input('place');
        $model->event_date = $request->input('event_date');
        $model->event_time = $request->input('event_time');
        $model->description = $request->input('description');

        $model->save();

        \Session::flash('flash-message','Cобытие добавлено');
        return redirect('/event');
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required'
        ]);

        $model = Event::find($id);
        $model->title = $request->input('title');
        $model->description = $request->input('description');
        $model->save();
        \Session::flash('flash-message','Cобытие сохранено');
        return redirect('/event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Event::find($id);
        $model->delete();
        $affectedRows = Member::where('event_id', '=', $id)->delete();

        \Session::flash('flash-message','Cобытие удалено');
        return redirect('/event');
    }
}
