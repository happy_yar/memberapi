<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\URL;

class RestAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return ($request->header('token')==12345 || $request->header('origin') || $request->header('referer'))?$next($request):response('Bad token!', 401);
    }
}
