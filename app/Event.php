<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table='events';

    protected $fillable=['title','place','descriptions'];

    public function scopeNear($query)
    {
        return $query->orderBy('event_date','desc')->orderBy('event_time','desc')->limit(3);
    }
}
