<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;

    protected $table='members';


    protected $fillable=['name','phone','email','male','birthday','event_id'];
    protected $dates = ['deleted_at'];

    /**
     * Get the event record associated with the member.
     */
    public function event()
    {
        return $this->hasOne('App\Event','id', 'event_id');
    }
    public function getMaleAttribute($value)
    {
        return ($value==1?"Муж":"Жен");
    }
}
