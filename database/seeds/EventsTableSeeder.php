<?php

use App\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Event::create([
            'title'=>'Концерт группы Kodaline',
            'place'=>'Yotaspace',
            'event_date'=>'2016-03-19',
            'event_time'=>'20:00:00',
            'description'=>'Похоже, будущие стадионные звезды из Ирландии',
        ]);
    }
}
