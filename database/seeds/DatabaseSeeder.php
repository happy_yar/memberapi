<?php

use App\Event;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(EventsTableSeeder::class);

        DB::table('events')->delete();

        $events[] =[
            'title'=>'Концерт группы Kodaline',
            'place'=>'Yotaspace',
            'event_date'=>'2016-03-19',
            'event_time'=>'20:00:00',
            'description'=>'Похоже, будущие стадионные звезды из Ирландии',
        ];
        $events[] =[
            'title'=>'Концерт «Танцы в «Петровиче»: DJ Влад Никулин',
            'place'=>'клуб Петрович',
            'event_date'=>'2016-03-19',
            'event_time'=>'00:00:00',
            'description'=>'Поп',
        ];
        $events[] =[
            'title'=>'Концерт «Midnight Jazz»: Mambo Jazz Party',
            'place'=>'клуб Эссе',
            'event_date'=>'2016-03-19',
            'event_time'=>'00:00:00',
            'description'=>'Джаз',
        ];
        $events[] = [
            'title'=>'Концерт Марина Девятова',
            'place'=>'Концертный зал Меридиан',
            'event_date'=>'2016-03-19',
            'event_time'=>'19:00:00',
            'description'=>'Поп, фолк',
        ];
        $events[] = [
            'title'=>'Концерт «СБПЧ»',
            'place'=>'Концертный зал Red',
            'event_date'=>'2016-03-19',
            'event_time'=>'20:00:00',
            'description'=>'О группе Кирилла Иванова любят говорить, что она регулярно меняется. Однако, главное уже долгое время неизменно — подкупающая своей наивностью прямота детской речи, где все нужно называть своими именами и никого не стесняться. Новая программа «Вечный взрыв», одноименная песне с последнего альбома «3 миллиарда ватт», по заверению группы разложит вас на атомы и соберет заново. И это едва ли преувеличение.',
        ];

        foreach ($events as $event)
            Event::create($event);


        $this->command->info('Таблица событий заполнена данными!');
    }
}
