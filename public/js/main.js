$( "#submit" ).click(function() {

    normalizeForm();

    var event_id = $('#event_id').val();

    var data = {
        name:$('#name').val(),
        email:$('#email').val(),
        event_id:event_id,
        phone:$('#phone').val(),
        male:$('#male').val(),
        birthday:$('#birthday').val(),
        _token: $('#token').val()
    };

    $.ajax({
        type:"POST",
        url:'rest/members',
        data:data,
        dataType: 'json',
        statusCode: {
            201: function() {
                window.location.href = '/member/join/'+event_id;
            }
        },
        error: function(data){
            var error = data.responseJSON;
            addError(error.validation_errors);
        }
    });
});

function addError(obj){
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            $("#"+key+"_group").addClass('has-error');
            $("#"+key+"_error").html(obj[key]);
        }
    }
}

function normalizeForm(){
    $(".form-group").removeClass('has-error');
    $(".help-block").each(function() {
        $( this ).text("");
    });
}
function getOptionsEvents() {

    $.get( "rest/events", function( data ) {
        console.log(data[1]);
        var opt= "<option value=''>Выбрать</option>";

        for(var i = 0; (i<data.length); i++){
          opt = opt + "<option value='"+data[i].id+"'>"+data[i].title+"</option>";
        }

        $("#event_id").html(opt);
    });
}

