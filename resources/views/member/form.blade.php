@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.join_event') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/') }}">
                            <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div id="name_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    <span id="name_error" class="help-block"></span>
                                </div>
                            </div>


                            <div id="email_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.email') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    <span id="email_error" class="help-block"></span>
                                </div>
                            </div>

                            <div id="phone_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.phone') }}</label>

                                <div class="col-md-6">
                                    <input id="phone" placeholder="+79999999999" class="form-control" name="email" value="{{ old('phone') }}">
                                    <span id="phone_error" class="help-block"></span>
                                </div>
                            </div>


                            <div id="male_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.male') }}</label>

                                <div class="col-md-6">
                                    <select id="male" name="male" class="form-control">
                                        <option value="">{{ trans('main.select') }}</option>
                                        <option value="1" @if(old('male')==1) selected @endif >Муж.</option>
                                        <option value="2" @if(old('male')==2) selected @endif >Жен.</option>
                                    </select>
                                    <span id="male_error" class="help-block"></span>
                                </div>
                            </div>

                            <div id="birthday_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.birthday') }}</label>

                                <div class="col-md-6">
                                    <input id="birthday" type="date" class="form-control" name="birthday" value="{{ old('birthday') }}">
                                    <span id="birthday_error" class="help-block"></span>
                                </div>
                            </div>


                            <div  id="event_id_group" class="form-group">
                                <label class="col-md-4 control-label">{{ trans('auth.event') }}</label>

                                <div class="col-md-6">
                                    <select id="event_id" name="event_id" class="form-control"></select>
                                    <span id="event_id_error" class="help-block"></span>
                                </div>
                            </div>

                        </form>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button id="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>{{ trans('main.join') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    getOptionsEvents();
@endsection


