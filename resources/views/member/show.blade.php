@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.member') }}</div>
                    <div class="panel-body">
                        <a style="float: right;" href="/member/edit/{{ $model->id }}"><i class="fa fa-btn fa-edit"></i>{{ trans('main.edit') }}</a>
                        <a style="float: right;margin-right: 20px;" href="/member/del/{{ $model->id }}"><i class="fa fa-btn fa-remove"></i>{{ trans('main.delete') }}</a>
                        <h2>
                            {{$model->name}}
                        </h2>
                        <table class="table">
                            <tr>
                                <td>{{ trans('auth.email') }}</td>
                                <td>{{$model->email}}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('auth.phone') }}</td>
                                <td>{{$model->phone}}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('auth.birthday') }}</td>
                                <td>{{$model->birthday}}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('auth.male') }}</td>
                                <td>{{$model->male}}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('auth.event') }}</td>
                                <td>{{$model->event->title}}</td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection