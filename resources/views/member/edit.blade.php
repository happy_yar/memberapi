@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.edit') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/member/update/{{ $model->id }}">
                            <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.name') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $model->name }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.email') }}</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $model->email }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.phone') }}</label>

                                <div class="col-md-6">
                                    <input placeholder="+79999999999" class="form-control" name="phone" value="{{ $model->phone }}">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('male') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.male') }}</label>

                                <div class="col-md-6">
                                    <select id="male" name="male" class="form-control">
                                        <option value="">{{ trans('main.select') }}</option>
                                        <option @if($model->male == "Муж.") selected @endif >Муж.</option>
                                        <option @if($model->male == "Жен.") selected @endif >Жен.</option>
                                    </select>
                                    @if ($errors->has('male'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('male') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.birthday') }}</label>

                                <div class="col-md-6">
                                    <input id="birthday" type="date" class="form-control" name="birthday" value="{{ $model->birthday }}">
                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('auth.event') }}</label>

                                <div class="col-md-6">
                                    <select id="event_id" name="event_id" class="form-control">
                                        <option value="">{{ trans('main.select') }}</option>
                                        @foreach($events as $event)
                                            <option @if($event->id == $model->event_id) selected @endif value="{{$event->id}}">{{$event->title}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('event_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('event_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>{{ trans('main.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
