@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.list_member') }}</div>

                    <div class="panel-body">
                        <ul>
                            @foreach($models as $model)
                                <li><a href="/member/{{$model->id}}">{{$model->name}} [{{$model->event->title}}]</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
