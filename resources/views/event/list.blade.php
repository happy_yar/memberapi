@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.list_event') }}</div>

                    <div class="panel-body">
                        <a style="float: right;" href="event/create"><i class="fa fa-btn fa-edit"></i>{{ trans('main.create') }}</a>
                        <ul>
                            @foreach($events as $event)
                                <li><a href="event/{{$event->id}}">{{$event->title}} [{{$event->place}}]</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
