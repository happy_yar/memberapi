@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.list_event') }}</div>

                    <div class="panel-body">
                        <a style="float: right;" href="/event/edit/{{ $model->id }}"><i class="fa fa-btn fa-edit"></i>{{ trans('main.edit') }}</a>
                        <a style="float: right;margin-right: 20px;" href="/event/del/{{ $model->id }}"><i class="fa fa-btn fa-remove"></i>{{ trans('main.delete') }}</a>
                        <h1>{{$model->title}}</h1>
                        <table class="table">
                            <tr>
                                <td>{{trans('main.event_date')}}</td>
                                <td>{{$model->event_date}}</td>
                            </tr>
                            <tr>
                                <td>{{trans('main.event_time')}}</td>
                                <td>{{$model->event_time}}</td>
                            </tr>
                            <tr>
                                <td>{{trans('main.place')}}</td>
                                <td>{{$model->place}}</td>
                            </tr>
                            <tr>
                                <td>{{trans('main.description')}}</td>
                                <td>{{$model->description}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection