@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('main.create') }}</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="/event/store">
                            <input id="token" type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('main.event') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('place') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('main.place') }}</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="title" value="{{ old('place') }}">
                                    @if ($errors->has('place'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('place') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('event_date') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('main.event_date') }}</label>

                                <div class="col-md-6">
                                    <input type="date" class="form-control" name="title" value="{{ old('event_date') }}">
                                    @if ($errors->has('event_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('event_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('event_time') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('main.event_time') }}</label>

                                <div class="col-md-6">
                                    <input type="time" class="form-control" name="title" value="{{ old('event_time') }}">
                                    @if ($errors->has('event_time'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('event_time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">{{ trans('main.description') }}</label>

                                <div class="col-md-6">
                                    <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>{{ trans('main.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
