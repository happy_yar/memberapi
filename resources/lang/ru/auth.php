<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Такой пользователь не регистрировался.',
    'throttle' => 'Слишком много попыток входа в систему. Попробуйте чере :seconds секунд.',
    'login' => 'Вход',
    'email' => 'Почта',
    'phone' => 'Телефон',
    'birthday' => 'День рождение',
    'male' => 'Пол',
    'pass' => 'Пароль',
    'confirm_pass' => 'Подтвердить пароль',
    'forgot' => 'Забыли пароль?',
    'remember' => 'Запомнить меня',
    'register' => 'Регистрация',
    'logout' => 'Выход',
    'name' => 'Имя',
    'event' => 'Событие',

];
